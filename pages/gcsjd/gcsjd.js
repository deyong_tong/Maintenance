const app = getApp();

var tpid = ['KCYMFmJidhtFxWqgqOanxYJIKCFiMLBfBvsBj_jl9lE', 'Pdoi-osurewAKPVFo7UkfPHMC-wR_k8uvGG3BsmkNcY', 'ffRtOhQgIoJCgw7m4EEV1HJUO_P7OCJ-WddzYOMNWKI'];

import {
  $wuxDialog
} from '../../dist/index';


Page({

  /**
   * 页面的初始数据
   */
  data: {
    shuju: {}, //页面显示的数据内容
    cz: "", //执行什么操作
    imgurl: "", //故障图片地址
    qmurl: '',
    img: [], //故障图
    wxxq: [], //维修详情
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    if (app.globalData.employId && app.globalData.employId != '') {

    } else {
      app.employIdCallback = employId => {
        if (employId != '') {

        }
      }
    }
    var that = this;
    if (options.id == 0) {
      //小程序内页面传递过来的
      var da = options.d;
      da = JSON.parse(da);
      console.log(da);
      var img = da.img;
      img = img.replace(/'/g, '"');
      img = JSON.parse(img);
      var p = app.globalData.url + 'uploads/';
      for (var i = 0; i < img.length; i++) {
        img[i] = p + img[i];
      }
      var w = da.wxxm;
      if (typeof (w) == 'undefined') {
        w = [];
      } else {
        w = w.replace(/'/g, '"');
        w = JSON.parse(w);
      }


      this.setData({
        shuju: da,
        cz: options.mode,
        imgurl: app.globalData.url + 'uploads/',
        qmurl: app.globalData.url + 'qianming/',
        img: img,
        wxxq: w
      })
    } else {
      //订阅消息转过来的
      var d = {
        id: options.id
      };
      var u = app.globalData.url + "getweixiudanxq.php";
      app.http(u, d).then(res => {
        var da = res.dwx;
        var img = da.img;
        img = img.replace(/'/g, '"');
        img = JSON.parse(img);
        var p = app.globalData.url + 'uploads/';
        for (var i = 0; i < img.length; i++) {
          img[i] = p + img[i];
        }
        var w = da.wxxm;
        if (typeof (w) == 'undefined') {
          w = [];
        } else {
          w = w.replace(/'/g, '"');
          w = JSON.parse(w);
        }
        that.setData({
          shuju: da,
          cz: options.mode,
          imgurl: app.globalData.url + 'uploads/',
          qmurl: app.globalData.url + 'qianming/',
          img: img,
          wxxq: w
        })
      }).catch(res => {
        wx.reLaunch({
          url: '/pages/weixiu/weixiu'
        })
      }).finally(() => {
        console.log('finally:', "结束");
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  privateimgs: function (e) { //预览图片
    wx.requestSubscribeMessage({
      tmplIds: tpid
    });

    var id = e.currentTarget.dataset.id;
    let imgs = this.data.img;
    wx.previewImage({
      current: imgs[id],
      urls: imgs
    })
  },
  bodadianhua: function (e) { //拨打电话
    wx.requestSubscribeMessage({
      tmplIds: tpid
    });
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.tel
    })
  },
  gotocaozuo: function (e) { //执行操作
    console.log(e);

    wx.requestSubscribeMessage({
      tmplIds: tpid
    });

    var that = this;
    var m = e.currentTarget.dataset.cz;
    var id = e.currentTarget.dataset.id;
    var uid = wx.getStorageSync('uid');
    if (m == 'qx') { //取消操作
      wx.reLaunch({
        url: '/pages/weixiu/weixiu'
      })
    } else {
      if (m == 'jieshu') { //管理员结单
        var zt = 12;
        $wuxDialog().prompt({
          resetOnClose: true,
          title: '提示',
          content: '确定执行结单操作？',
          fieldtype: 'text',
          password: 0,
          defaultText: '',
          placeholder: '请输入处理结果(选填)',
          maxlength: 140,
          onConfirm(e, response) {
            console.log("确定 ", response);
            let u = app.globalData.url + 'glyjd.php';
            let d = {
              uid: uid,
              id: id,
              zt: zt,
              yj: response
            }
            app.http(u, d).then(res => {
              wx.showToast({
                title: '成功',
                icon: 'success',
                duration: 2000
              });
              setTimeout(function () {
                wx.reLaunch({
                  url: '/pages/weixiu/weixiu'
                })
              }, 2000)
            }).catch(res => {
              console.log('fail:', res);
            }).finally(() => {
              console.log('finally:', "结束");
            })
          },
          onCancel(e, response) {
            console.log("取消 ")
          }
        })
      } else if (m == 'jie') { //工程师接单

        var zt = 5;
        $wuxDialog().prompt({
          resetOnClose: true,
          title: '提示',
          content: '确定执行接单操作？',
          fieldtype: 'text',
          password: 0,
          defaultText: '',
          placeholder: '请输入接单意见(选填)',
          maxlength: 140,
          onConfirm(e, response) {
            console.log("确定 ", response);
            let u = app.globalData.url + 'gcsjd.php';
            let d = {
              uid: uid,
              id: id,
              zt: zt,
              yj: response
            }
            app.http(u, d).then(res => {
              wx.showToast({
                title: '成功',
                icon: 'success',
                duration: 2000
              });
              setTimeout(function () {
                wx.reLaunch({
                  url: '/pages/weixiu/weixiu'
                })
              }, 2000)
            }).catch(res => {
              console.log('fail:', res);
            }).finally(() => {
              console.log('finally:', "结束");
            })
          },
          onCancel(e, response) {
            console.log("取消 ")
          }
        })

      } else if (m == 'jujue') { //工程师拒单
        var zt = 4;
        $wuxDialog().prompt({
          resetOnClose: true,
          title: '提示',
          content: '确定执行接单操作？',
          fieldtype: 'text',
          password: 0,
          defaultText: '',
          placeholder: '请输入拒单原因(选填)',
          maxlength: 140,
          onConfirm(e, response) {
            console.log("确定 ", response);
            let u = app.globalData.url + 'gcsjd.php';
            let d = {
              uid: uid,
              id: id,
              zt: zt,
              yj: response
            }
            app.http(u, d).then(res => {
              wx.showToast({
                title: '成功',
                icon: 'success',
                duration: 2000
              });
              setTimeout(function () {
                wx.reLaunch({
                  url: '/pages/weixiu/weixiu'
                })
              }, 2000)
            }).catch(res => {
              console.log('fail:', res);
            }).finally(() => {
              console.log('finally:', "结束");
            })
          },
          onCancel(e, response) {
            console.log("取消 ")
          }
        })

      } else if (m == 'wc') { //工程师维修完成
        var qm = wx.getStorageSync('qm');
        if (qm == 0) {
          wx.showModal({
            title: '提示',
            content: '您还未签名，无法进行此操作。点击确定，前往签名。',
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定');
                wx.reLaunch({
                  url: '/pages/index/canvas1?mode=qm&id=0'
                })
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else {
          var wxid = e.currentTarget.dataset.id;
          wx.showModal({
            title: '提示',
            content: '请让接受维修者签名确认维修结果。',
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定');
                wx.reLaunch({
                  url: '/pages/index/canvas2?mode=wx&id=' + wxid
                })
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      } else if (m == 'xw') { //填写维修项目
        var wxid = e.currentTarget.dataset.id;
        wx.reLaunch({
          url: '/pages/wxxm/wxxm?d=[]&id=' + wxid
        })
      } else if (m == 'xg') { //修改维修项目
        var xq = that.data.wxxq;
        var wxid = e.currentTarget.dataset.id;
        wx.reLaunch({
          url: '/pages/wxxm/wxxm?d=' + JSON.stringify(xq) + '&id=' + wxid
        })
      }
    }
  },
})