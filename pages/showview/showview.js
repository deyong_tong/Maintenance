const app = getApp();
import {
  $wuxDialog
} from '../../dist/index';


Page({

  /**
   * 页面的初始数据
   */
  data: {
    shuju: {}, //页面显示的数据内容
    cz: "", //执行什么操作
    imgurl: "", //故障图片地址
    qmurl: '',
    img: [], //故障图
    wxxq: [], //维修详情
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    if (app.globalData.employId && app.globalData.employId != '') {

    } else {
      app.employIdCallback = employId => {
        if (employId != '') {

        }
      }
    }
    var that = this;
    if (options.id == 0) {
      //小程序内页面传递过来的
      var da = options.d;
      da = JSON.parse(da);
      console.log(da);
      var img = da.img;
      img = img.replace(/'/g, '"');
      img = JSON.parse(img);
      var p = app.globalData.url + 'uploads/';
      for (var i = 0; i < img.length; i++) {
        img[i] = p + img[i];
      }
      var w = da.wxxm;
      console.log(w)
      if (typeof (w) == 'undefined' || w=='') { } else {
        w = w.replace(/'/g, '"');
        w = JSON.parse(w);
      }


      this.setData({
        shuju: da,
        cz: options.mode,
        imgurl: app.globalData.url + 'uploads/',
        qmurl: app.globalData.url + 'qianming/',
        img: img,
        wxxq: w
      })
    } else {
      //订阅消息转过来的
      var d = {
        id: options.id
      };
      var u = app.globalData.url + "getweixiudanxq.php";
      app.http(u, d).then(res => {
        var da = res.dwx;
        var img = da.img;
        img = img.replace(/'/g, '"');
        img = JSON.parse(img);
        var p = app.globalData.url + 'uploads/';
        for (var i = 0; i < img.length; i++) {
          img[i] = p + img[i];
        }
        var w = da.wxxm;
        if (typeof (w) == 'undefined' || w == '') { } else {
          w = w.replace(/'/g, '"');
          w = JSON.parse(w);
        }
        that.setData({
          shuju: da,
          cz: options.mode,
          imgurl: app.globalData.url + 'uploads/',
          qmurl: app.globalData.url + 'qianming/',
          img: img,
          wxxq: w
        })
      }).catch(res => {
        wx.reLaunch({
          url: '/pages/weixiu/weixiu'
        })
      }).finally(() => {
        console.log('finally:', "结束");
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  privateimgs: function (e) { //预览图片
    var id = e.currentTarget.dataset.id;
    let imgs = this.data.img;
    wx.previewImage({
      current: imgs[id],
      urls: imgs
    })
  },
  bodadianhua: function (e) { //拨打电话
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.tel
    })
  },
})