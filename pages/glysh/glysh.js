const app = getApp();
var tpid = ['UZ985nAiJPyFHRgnH1Trub_uV6WQUuejRfy4BweuGpI', 'WFWgUVkLuYcRcDJewxMEgDuwP0e4uvC6E2cnjQAkTX4', 'Pdoi-osurewAKPVFo7UkfPHMC-wR_k8uvGG3BsmkNcY'];
import {
  $wuxDialog
} from '../../dist/index';
var istj = false;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    shuju: {}, //页面显示的数据内容
    cz: "", //执行什么操作
    imgurl: "", //故障图片地址
    qmurl: '',
    img: [], //故障图
    paidan: false, //派单窗口
    clyj: '', //处理意见
    value1: '',
    displayValue1: '请选择',
    options1: [],
    wxxq: [], //维修详情

  },


  setValue(values, key) {
    this.setData({
      [`value${key}`]: values.value,
      [`displayValue${key}`]: values.label,
    })
  },
  onConfirm(e) {
    const {
      index
    } = e.currentTarget.dataset
    this.setValue(e.detail, index)
    console.log(`onConfirm${index}`, e.detail)
  },
  onValueChange(e) {
    const {
      index
    } = e.currentTarget.dataset
    console.log(`onValueChange${index}`, e.detail)
  },
  onVisibleChange(e) {
    this.setData({
      visible: e.detail.visible
    })
  },
  onClick() {
    this.setData({
      visible: true
    })
  },





  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    if (app.globalData.employId && app.globalData.employId != '') {

    } else {
      app.employIdCallback = employId => {
        if (employId != '') {

        }
      }
    }
    var that = this;
    if (options.id == 0) {
      //小程序内页面传递过来的
      var da = options.d;
      da = JSON.parse(da);
      console.log(da);
      var img = da.img;
      img = img.replace(/'/g, '"');
      img = JSON.parse(img);
      var p = app.globalData.url + 'uploads/';
      for (var i = 0; i < img.length; i++) {
        img[i] = p + img[i];
      }
      var w = da.wxxm;
      if (typeof (w) == 'undefined' || w == '') { } else {
        w = w.replace(/'/g, '"');
        w = JSON.parse(w);
      }

      this.setData({
        shuju: da,
        cz: options.mode,
        imgurl: app.globalData.url + 'uploads/',
        qmurl: app.globalData.url + 'qianming/',
        img: img,
        wxxq: w
      })
    } else {
      //订阅消息转过来的
      var d = {
        id: options.id
      };
      var u = app.globalData.url + "getdingdanxq.php";
      app.http(u, d).then(res => {
        var da = res.dsh;
        var img = da.img;
        img = img.replace(/'/g, '"');
        img = JSON.parse(img);
        var p = app.globalData.url + 'uploads/';
        for (var i = 0; i < img.length; i++) {
          img[i] = p + img[i];
        }
        var w = da.wxxm;
        if (typeof (w) == 'undefined' || w == '') { } else {
          w = w.replace(/'/g, '"');
          w = JSON.parse(w);
        }
        that.setData({
          shuju: da,
          cz: options.mode,
          imgurl: app.globalData.url + 'uploads/',
          qmurl: app.globalData.url + 'qianming/',
          img: img,
          wxxq: w
        })
      }).catch(res => {
        wx.reLaunch({
          url: '/pages/guanli/guanli'
        })
      }).finally(() => {
        console.log('finally:', "结束");
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  privateimgs: function (e) { //预览图片
    wx.requestSubscribeMessage({
      tmplIds: tpid
    });
    var id = e.currentTarget.dataset.id;
    let imgs = this.data.img;
    wx.previewImage({
      current: imgs[id],
      urls: imgs
    })
  },
  bodadianhua: function (e) { //拨打电话
    wx.requestSubscribeMessage({
      tmplIds: tpid
    });
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.tel
    })
  },
  gotocaozuo: function (e) { //执行操作
    wx.requestSubscribeMessage({
      tmplIds: tpid
    });
    console.log(e);
    var that = this;
    var cz = e.currentTarget.dataset.cz;
    var id = e.currentTarget.dataset.id;
    var uid = wx.getStorageSync('uid');
    var yzt = e.currentTarget.dataset.yzt;
    if (cz == 'qx') { //取消操作
      wx.reLaunch({
        url: '/pages/guanli/guanli'
      })
    } else {
      if (cz == 'th') { //退回操作
        var zt = 1;
        $wuxDialog().prompt({
          resetOnClose: true,
          title: '提示',
          content: '确定执行退回操作？',
          fieldtype: 'text',
          password: 0,
          defaultText: '',
          placeholder: '请输入处理意见(选填)',
          maxlength: 140,
          onConfirm(e, response) {
            console.log("确定 ", response);
            console.log('onChange', e);
            let u = app.globalData.url + 'tdhegd.php';
            let d = {
              uid: uid,
              id: id,
              zt: zt,
              yj: response
            }
            app.http(u, d).then(res => {
              wx.showToast({
                title: '成功',
                icon: 'success',
                duration: 2000
              });
              setTimeout(function () {
                wx.reLaunch({
                  url: '/pages/guanli/guanli'
                })
              }, 2000)
            }).catch(res => {
              console.log('fail:', res);
            }).finally(() => {
              console.log('finally:', "结束");
            })
          },
          onCancel(e, response) {
            console.log("取消 ")
          }
        })
      } else if (cz == 'gd') { //关单操作
        var zt = 14;
        $wuxDialog().prompt({
          resetOnClose: true,
          title: '提示',
          content: '确定执行关单操作？',
          fieldtype: 'text',
          password: 0,
          defaultText: '',
          placeholder: '请输入处理意见(选填)',
          maxlength: 140,
          onConfirm(e, response) {
            console.log("确定 ", response);
            let u = app.globalData.url + 'tdhegd.php';
            let d = {
              uid: uid,
              id: id,
              zt: zt,
              yj: response
            };
            app.http(u, d).then(res => {
              wx.showToast({
                title: '成功',
                icon: 'success',
                duration: 2000
              });
              setTimeout(function () {
                wx.reLaunch({
                  url: '/pages/guanli/guanli'
                })
              }, 2000)
            }).catch(res => {
              console.log('fail:', res);
            }).finally(() => {
              console.log('finally:', "结束");
            })
          },
          onCancel(e, response) {
            console.log("取消 ")
          }
        })
      } else if (cz == 'pd') { //派单
        let u = app.globalData.url + "getgongchengshi.php";
        app.http(u).then(res => {
          console.log(res)
          that.setData({
            options1: res.data,
            paidan: true
          })
        }).catch(res => {
          console.log('fail:', res);
        }).finally(() => {
          console.log('finally:', "结束");
        })
      } else if (cz == 'bh') { //费用审核驳回
        var qm = wx.getStorageSync('qm');
        if (qm == 0) {
          wx.showModal({
            title: '提示',
            content: '您还未签名，无法进行此操作。点击确定，前往签名。',
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定');
                wx.reLaunch({
                  url: '/pages/index/canvas2?mode=qm&id=0'
                })
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else {

          var zt = 7;
          $wuxDialog().prompt({
            resetOnClose: true,
            title: '提示',
            content: '确定执行驳回操作？',
            fieldtype: 'text',
            password: 0,
            defaultText: '',
            placeholder: '请输入驳回原因(选填)',
            maxlength: 140,
            onConfirm(e, response) {
              console.log("确定 ", response);
              let u = app.globalData.url + 'bohuifeiyongsq.php';
              let d = {
                uid: uid,
                id: id,
                zt: zt,
                yj: response,
                yzt: yzt
              };
              app.http(u, d).then(res => {
                wx.showToast({
                  title: '成功',
                  icon: 'success',
                  duration: 2000
                });
                setTimeout(function () {
                  wx.reLaunch({
                    url: '/pages/guanli/guanli'
                  })
                }, 2000)
              }).catch(res => {
                console.log('fail:', res);
              }).finally(() => {
                console.log('finally:', "结束");
              })
            },
            onCancel(e, response) {
              console.log("取消 ")
            }
          })

        }
      } else if (cz == 'tg') { //费用审核通过
        var qm = wx.getStorageSync('qm');
        if (qm == 0) {
          wx.showModal({
            title: '提示',
            content: '您还未签名，无法进行此操作。点击确定，前往签名。',
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定');
                wx.reLaunch({
                  url: '/pages/index/canvas2?mode=qm&id=0'
                })
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else {
          var zt = 8;
          $wuxDialog().prompt({
            resetOnClose: true,
            title: '提示',
            content: '确定执行通过操作？',
            fieldtype: 'text',
            password: 0,
            defaultText: '',
            placeholder: '请输入处理意见(选填)',
            maxlength: 140,
            onConfirm(e, response) {
              console.log("确定 ", response);
              let u = app.globalData.url + 'bohuifeiyongsq.php';
              let d = {
                uid: uid,
                id: id,
                zt: zt,
                yj: response,
                yzt: yzt
              };
              app.http(u, d).then(res => {
                wx.showToast({
                  title: '成功',
                  icon: 'success',
                  duration: 2000
                });
                setTimeout(function () {
                  wx.reLaunch({
                    url: '/pages/guanli/guanli'
                  })
                }, 2000)
              }).catch(res => {
                console.log('fail:', res);
              }).finally(() => {
                console.log('finally:', "结束");
              })
            },
            onCancel(e, response) {
              console.log("取消 ")
            }
          })
        }
      }
    }
  },
  shuruyijian: function (e) { //输入意见
    let b = e.detail.value;
    b = b.replace('"','“');
    b = b.replace("'","‘");
    b = b.replace(',','，');
    b = b.replace(/[\r]/g, "");
    b = b.replace(/[\n]/g, "");
    this.setData({
      clyj: b
    });
  },
  quxiaobtn: function () { //关闭派单窗口
    this.setData({
      paidan: false
    })
  },
  quedingbtn: function (e) { //确定按钮
    if (istj) {
      return;
    }
    istj = true;
    var id = e.currentTarget.dataset.id;
    var clyj = this.data.clyj;
    var v = this.data.value1;
    var r = this.data.displayValue1;
    console.log(id, clyj, v, r);
    if (v == '') {
      istj = false;
      return;
    }
    if (r.search("信息中心") == -1) {
      var zt = 3;
    } else {
      var zt = 2;
    }
    console.log(zt);
    var status = this.data.shuju.status;
    console.log("原始状态", status)
    var uid = wx.getStorageSync('uid');
    let u = app.globalData.url + "paidan.php";
    let d = {
      uid: uid,
      gcs: v,
      zt: zt,
      yj: clyj,
      id: id,
      status: status
    };
    var that = this;
    app.http(u, d).then(res => {
      wx.showToast({
        title: '成功',
        icon: 'success',
        duration: 2000
      });
      that.setData({
        paidan: false, //派单窗口
        clyj: '', //处理意见
        value1: '',
        displayValue1: '请选择',
        options1: []
      });
      setTimeout(function () {
        wx.reLaunch({
          url: '/pages/guanli/guanli'
        })
      }, 2000)
    }).catch(res => {
      console.log('fail:', res);
    }).finally(() => {
      istj = false;
      console.log('finally:', "结束");
    })


  }
})