const app = getApp();
var istj = false;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    viewtop: 85, //搜索viewTOP值
    jyss: true, //禁用搜索按钮
    uname: '', //搜索框输入的内容
    showuser: false, //是否显示信息预览
    userdata: {} //服务器返回的用户信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var jn = wx.getMenuButtonBoundingClientRect();
    this.setData({
      viewtop: jn.bottom + 10
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  inputtext: function (e) {
    var s = e.detail.value;
    if (s.length == 11) {
      var jyan = false;
    } else {
      var jyan = true;
    }
    this.setData({
      uname: s,
      jyss: jyan
    })
  },
  sousuozz: function () {
    var that = this;
    console.log(that.data.uname);
    var u = app.globalData.url + "searchuser.php";
    var d = {
      uname: that.data.uname
    }
    app.http(u, d).then(res => {
      that.setData({
        showuser: true,
        userdata: res
      })
    }).catch(res => {
      console.log('fail:', res);
      that.setData({
        showuser: false
      })
    }).finally(() => {
      console.log('finally:', "结束");
    })


  },
  guerenzhuce: function () {
    if (istj) {
      return;
    }
    istj = true;
    var that = this;


    if (that.data.userdata.zt == "1") {
      var url = '/pages/baoxiu/baoxiu';
      var tpid = ['KCYMFmJidhtFxWqgqOanxYJIKCFiMLBfBvsBj_jl9lE', 'IFTXxnpNSPkiN3H1WH4XTPlrzlDrq1ynJPNsIdUTLYs', 'ffRtOhQgIoJCgw7m4EEV1HJUO_P7OCJ-WddzYOMNWKI'];
    } else if (that.data.userdata.zt == "2") {
      var url = '/pages/guanli/guanli';
      var tpid = ['UZ985nAiJPyFHRgnH1Trub_uV6WQUuejRfy4BweuGpI', 'WFWgUVkLuYcRcDJewxMEgDuwP0e4uvC6E2cnjQAkTX4', 'Pdoi-osurewAKPVFo7UkfPHMC-wR_k8uvGG3BsmkNcY'];
    } else if (that.data.userdata.zt == "3") {
      var url = '/pages/weixiu/weixiu';
      var tpid = ['Pdoi-osurewAKPVFo7UkfPHMC-wR_k8uvGG3BsmkNcY', 'KCYMFmJidhtFxWqgqOanxYJIKCFiMLBfBvsBj_jl9lE', 'ffRtOhQgIoJCgw7m4EEV1HJUO_P7OCJ-WddzYOMNWKI'];
    } else {
      wx.showModal({
        title: '提示',
        content: '无效角色，请与管理员联系。',
        showCancel: false,
        success(res) {
          if (res.confirm) {
            istj = false;
            return;
          }
        }
      })
    }

    wx.requestSubscribeMessage({
      tmplIds: tpid,
      success(res) {
        console.log("success", res);


        var id = that.data.userdata.id;
        var d = {
          uid: wx.getStorageSync('uid'),
          id: id
        };
        var u = app.globalData.url + "reg.php";
        app.http(u, d).then(res => {
          wx.showToast({
            title: '注册成功',
            icon: 'success',
            duration: 2000
          });
          wx.setStorageSync('zt', that.data.userdata.zt);
          setTimeout(() => {
            wx.reLaunch({
              url: url
            })
          }, 2000);
        }).catch(res => {
          console.log('fail:', res);
        }).finally(() => {
          console.log('finally:', "结束");
          istj = false;
        })



      },
      fail(res) {
        console.log("fail", res);
        istj = false;
      }
    })
  }
})