var tpid1 = ['Pdoi-osurewAKPVFo7UkfPHMC-wR_k8uvGG3BsmkNcY', 'UZ985nAiJPyFHRgnH1Trub_uV6WQUuejRfy4BweuGpI', 'WFWgUVkLuYcRcDJewxMEgDuwP0e4uvC6E2cnjQAkTX4'];
var tpid2 = ['IFTXxnpNSPkiN3H1WH4XTPlrzlDrq1ynJPNsIdUTLYs', 'UZ985nAiJPyFHRgnH1Trub_uV6WQUuejRfy4BweuGpI', 'Pdoi-osurewAKPVFo7UkfPHMC-wR_k8uvGG3BsmkNcY'];
var tpid3 = ['IFTXxnpNSPkiN3H1WH4XTPlrzlDrq1ynJPNsIdUTLYs', 'WFWgUVkLuYcRcDJewxMEgDuwP0e4uvC6E2cnjQAkTX4', 'Pdoi-osurewAKPVFo7UkfPHMC-wR_k8uvGG3BsmkNcY'];
const app = getApp();
import {
  $wuxDialog
} from '../../dist/index';
const isTel = (value) => !/^1[3456789]\d{9}$/.test(value);


Page({

  /**
   * 页面的初始数据
   */
  data: {
    current: '1', //默认显示的tabbar
    tit: ["记录", "审核", "我的"],
    appname: "报修平台",
    jntop: 0, //胶囊按钮TOP
    jnheight: 0, //胶囊按钮高
    jnbottom: 0, //胶囊按钮bottom
    juese: 0, //角色
    tel: '',
    xgtel: '',
    dwm: '',
    xgdwm: '',
    lxrr: '',
    xglxrr: '',
    visibleqm: false, //是否预览签名
    myqmurl: '', //我的签名图片地址
    dbnum: 0, //待办数量（暂未用）
    xingongdan: [], //报修者提报的待审核工单
    judan: [], //工程师拒单数据
    xiuwan: [], //工程师维修完成，待审核费用的单子
    shlist: [], //审核历史列表
    animations: [
      'fadeIn',
      'fadeInDown',
      'fadeInLeft',
      'fadeInRight',
      'fadeInUp',
      'slideInUp',
      'slideInDown',
      'slideInLeft',
      'slideInRight',
      'zoom',
      'punch',
      'custom',
    ], //动画
    example: {
      animation: 'fadeIn',
      classNames: 'wux-animate--fadeIn',
      enter: true,
      exit: true,
      in: false,
    }, //动画
    options: [], //选择单位
    value9: [], //选择单位
    visibledw: false, //选择单位
  },
  onCloseqm() { //关闭签名预览
    this.setData({
      visibleqm: false
    })
  },
  qianming: function (e) { //预览（重设）签名

    var tpid = [tpid1, tpid2, tpid3];
    var x = Math.floor(Math.random()*(2+1));
    wx.requestSubscribeMessage({
      tmplIds: tpid[x]
    })

    var youqianming = wx.getStorageSync('qm');
    if (youqianming) {
      this.setData({
        visibleqm: true
      })
    } else {
      wx.navigateTo({
        url: '/pages/index/canvas2?mode=qm&id=0'
      })
    }
  },
  chongxieqianming: function () {
    wx.navigateTo({
      url: '/pages/index/canvas2?mode=qm&id=0'
    })
  },


  onChange(e) { //TabBar选项卡切换
    console.log('onChange', e)
    if (e.detail.key == 1) {
      this.getDaiShenHe();
    }
    if (e.detail.key == 0) {
      this.getShenHeList();
    }
    this.setData({
      current: e.detail.key,
      'example.in': false //关闭修改资料
    })
    var tpid = [tpid1, tpid2, tpid3];
    wx.requestSubscribeMessage({
      tmplIds: tpid[e.detail.key]
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var nm = wx.getStorageSync('config');
    this.setData({
      appname: nm.appname
    })
    if (this.data.current == 1) {
      this.getDaiShenHe();
    }
    if (this.data.current == 0) {
      this.getShenHeList();
    }
  },

  getDaiShenHe: function () { //获取待审核列表
    var u = app.globalData.url + "getdaishenhe.php";
    var d = {
      uid: wx.getStorageSync('uid')
    }
    var that = this;
    app.http(u, d).then(res => {
      console.log(res);
      that.setData({
        xingongdan: res.dsh,
        judan: res.dpd,
        xiuwan: res.dfk
      })
    }).catch(res => {
      console.log('fail:', res);
    }).finally(() => {
      console.log('finally:', "结束");
    })
  },
  getShenHeList: function () { //获取审核历史列表
    var u = app.globalData.url + "getshenhelist.php";
    var d = {
      uid: wx.getStorageSync('uid')
    }
    var that = this;
    app.http(u, d).then(res => {
      console.log(res);
      that.setData({
        shlist: res.data
      })
    }).catch(res => {
      console.log('fail:', res);
    }).finally(() => {
      console.log('finally:', "结束");
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    let u = app.globalData.url + 'getuserinfo.php';
    let d = {
      uid: wx.getStorageSync('uid')
    }
    app.http(u, d).then(res => {
      if (res.qm != '0') {
        var youqianming = 1;
        var qm = res.qm;
      } else {
        var qm = '';
        var youqianming = 0;
      }
      wx.setStorageSync('qm', youqianming);
      var dw = [];
      dw.push(res.dwid);
      that.setData({
        tel: res.tel,
        dwm: res.dw,
        lxrr: res.ming,
        myqmurl: qm,
        value9: dw
      })
    }).catch(res => {
      console.log('fail:', res);
    }).finally(() => {
      console.log('finally:', "结束");
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var jn = wx.getMenuButtonBoundingClientRect();
    console.log(jn)
    this.setData({
      jntop: jn.top,
      jnheight: jn.height,
      jnbottom: jn.bottom,
      juese: wx.getStorageSync("zt")
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad();
    this.onReady();
    this.onShow();
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  changejuese: function () { //切换角色
    var tpid = [tpid1, tpid2, tpid3];
    var x = Math.floor(Math.random()*(2+1));
    wx.requestSubscribeMessage({
      tmplIds: tpid[x]
    })

    var that = this;
    $wuxDialog().open({
      resetOnClose: true,
      title: '选择新的角色',
      verticalButtons: !0,
      buttons: [{
          text: '报修者',
          bold: !0,
          onTap(e) {
            wx.reLaunch({
              url: '/pages/baoxiu/baoxiu'
            })
          },
        },
        {
          text: '工程师',
          bold: !0,
          onTap(e) {
            wx.reLaunch({
              url: '/pages/weixiu/weixiu'
            })
          },
        },
        {
          text: '取消',
          bold: 0
        }
      ],
    })
  },
  dianjikapian: function (e) {//点击卡片
    var tpid = [tpid1, tpid2, tpid3];
    var x = Math.floor(Math.random()*(2+1));
    wx.requestSubscribeMessage({
      tmplIds: tpid[x]
    })
    console.log(e)
    var idx = e.currentTarget.dataset.index; //索引
    var id = e.currentTarget.dataset.id; //工单ID
    var mod = e.currentTarget.dataset.mode;
    console.log(idx, id, mod);
    if (mod == 'xdsh') {
      var dsh = this.data.xingongdan;
      var data = JSON.stringify(dsh[idx]);
    }
    if (mod == 'jdth') {
      var dsh = this.data.judan;
      var data = JSON.stringify(dsh[idx]);
    }
    if (mod == 'wxwc') {
      var dsh = this.data.xiuwan;
      var data = JSON.stringify(dsh[idx]);
    }
    if (mod == 'shls') {
      var dsh = this.data.shlist;
      var data = JSON.stringify(dsh[idx]);
      wx.navigateTo({
        url: '/pages/showview/showview?mode=' + mod + '&d=' + data + '&id=0'
      })
      return false;
    }

    wx.reLaunch({
      url: '/pages/glysh/glysh?mode=' + mod + '&d=' + data + '&id=0'
    })

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var uid = wx.getStorageSync("uid");
    var n = this.data.appname;
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '欢迎使用' + n,
      path: '/pages/index/index',
      imageUrl: '/images/share.png',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },
  /**
   * 动画
   */
  onEnter(e) {
    console.log('onEnter', e.detail)
  },
  onEntering(e) {
    console.log('onEntering', e.detail)
  },
  onEntered(e) {
    console.log('onEntered', e.detail)

    // delay 300ms close animation
    setTimeout(() => this.setData({
      //'example.in': false
    }), 300)
  },
  onExit() {
    console.log('onExit')
  },
  onExiting() {
    console.log('onExiting')
  },
  onExited() {
    console.log('onExited')
  },
  onChangea(e) {
    console.log('onChange', e.detail)
  },
  /**
   * 动画
   */
  xiugaiziliao: function () { //修改资料

    var tpid = [tpid1, tpid2, tpid3];
    var x = Math.floor(Math.random()*(2+1));
    wx.requestSubscribeMessage({
      tmplIds: tpid[x]
    })


    //const { index } = e.currentTarget.dataset;
    //const animation = this.data.animations[index];
    const animation = 'zoom';
    //onst classNames = `wux-animate--${animation}`;
    const classNames = `wux-animate--zoom`;

    let u = app.globalData.url + 'getdanweilist.php';
    let d = {
      uid: wx.getStorageSync('uid')
    };
    var that = this;
    app.http(u, d).then(res => {
      that.setData({
        'example.in': true,
        'example.classNames': classNames,
        'example.animation': animation,
        xgtel: this.data.tel,
        xgdwm: this.data.dwm,
        xglxrr: this.data.lxrr,
        options: res.data
      })
    }).catch(res => {
      console.log('fail:', res);
    }).finally(() => {
      console.log('finally:', "结束");
    })
  },
  quxiaoxgzl: function () { //取消修改资料
    this.setData({
      'example.in': false
    });
  },
  querenxgzl: function () { //确认提交修改资料
    var that = this;
    var newname = that.data.xglxrr;
    var newdwid = that.data.value9;
    var newtel = that.data.xgtel;
    var ok = true;
    var msg = '';
    if (isTel(newtel)) {
      ok = false;
      msg = "请输入正确手机号";
    }
    if (newname.length < 2) {
      ok = false;
      msg = "请输入正确姓名";
    }
    if (ok) {
      let u = app.globalData.url + "changeuserinfo.php";
      let d = {
        uid: wx.getStorageSync('uid'),
        ming: newname,
        tel: newtel,
        dw: newdwid[0]
      };
      app.http(u, d).then(res => {
        that.setData({
          lxrr: that.data.xglxrr,
          tel: that.data.xgtel,
          dwm: that.data.xgdwm,
          'example.in': false
        })
      }).catch(res => {
        console.log('fail:', res);
      }).finally(() => {
        console.log('finally:', "结束");
      })

    } else {
      wx.showModal({
        title: '提示',
        content: msg,
        showCancel: false,
        success(res) {
          if (res.confirm) {
            return false;
          }
        }
      })
    }
  },
  selectdanwei: function (e) { //弹出选择单位
    this.setData({
      visibledw: true
    })
  },

  onConfirm(e) {
    const {
      index
    } = e.currentTarget.dataset
    this.setValue(e.detail, index)
    console.log(`onConfirm${index}`, e.detail)
  },
  setValue(values, key) {
    this.setData({
      [`value${key}`]: values.value,
      xgdwm: values.label,
    })
  },
  onVisibleChange(e) {
    this.setData({
      visibledw: e.detail.visible
    })
  },
  onValueChange(e) {
    const {
      index
    } = e.currentTarget.dataset
    console.log(`onValueChange${index}`, e.detail)
  },
  shuruzlname: function (e) {
    this.setData({
      xglxrr: e.detail.value
    })
  },
  shuruzltelll: function (e) {
    this.setData({
      xgtel: e.detail.value
    })
  },
  yinsi: function () {
    wx.navigateTo({
      url: '/pages/yinsi/yinsi',
    })
  },
})