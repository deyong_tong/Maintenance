const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    animations: [
      'fadeIn',
      'fadeInDown',
      'fadeInLeft',
      'fadeInRight',
      'fadeInUp',
      'slideInUp',
      'slideInDown',
      'slideInLeft',
      'slideInRight',
      'zoom',
      'punch',
      'custom',
    ], //动画
    example: {
      animation: 'fadeIn',
      classNames: 'wux-animate--fadeIn',
      enter: true,
      exit: true,
      in: false,
    }, //动画
    obj: [0], //动态添加组件
    weixiu: [{
      'xm': '',
      'dw': '',
      'dj': '',
      'sl': ''
    }], //维修详情(项目、单位、单价、数量)
    wxid: 0, //维修项目ID
  },
  /***增加组件 */
  onTapAdd: function(e) {
    var temp = this.data.obj;
    temp.push(this.data.obj.length);
    var t = {
      'xm': '',
      'dw': '',
      'dj': '',
      'sl': ''
    };
    var wx = this.data.weixiu;
    wx.push(t);
    this.setData({
      obj: temp,
      weixiu: wx
    });
    console.log(this.data.weixiu)
  },
  /***** 删除最后一个组件，也可以修改删除指定组件*/
  onTapDel: function(e) {
    var temp = this.data.obj;
    temp.pop(this.data.obj.length);
    this.setData({
      obj: temp
    })
  },
  shuruxinxi: function(e) { //输入维修信息
    console.log(e);
    var v = e.detail.value;
    var id = e.target.dataset.id;
    var md = e.target.dataset.mod;
    var wxx = this.data.weixiu;

    if (md == 'xm') {
      wxx[id].xm = v;
    } else if (md == 'dw') {
      wxx[id].dw = v;
    } else if (md == 'dj') {
      wxx[id].dj = v;
    } else if (md == 'sl') {
      wxx[id].sl = v;
    }

    this.setData({
      weixiu: wxx
    })
  },
  jisuanzongjia: function(e) { //确认维修信息，计算总价，提交数据库
    var that = this;
    var wxx = this.data.weixiu;
    var youxiao = [];
    var zongjia = 0;
    var s = '请确认项目及金额（仅有效输入）\r\n\r\n';
    console.log(wxx);
    for (var i = 0; i < wxx.length; i++) {
      if (wxx[i].xm != '' && wxx[i].dw != '') {
        if (wxx[i].dj != "" && !isNaN(wxx[i].dj)) {
          if (wxx[i].sl != "" && !isNaN(wxx[i].sl)) {
            console.log("可以", wxx[i]);
            youxiao.push(wxx[i]);
            var xj = wxx[i].dj * wxx[i].sl;
            s = s + wxx[i].xm + '(' + wxx[i].dw + ')_￥' + xj.toFixed(2) + '\r\n';
            zongjia = Number(zongjia) + Number(xj.toFixed(2));
          }
        }
      }
    }
    s = s + '\r\n总金额：￥' + zongjia;
    console.log(s);

    if (zongjia <= 0) {
      wx.showModal({
        title: '提示',
        content: "无有效数据",
        showCancel: false,
        success(res) {
          if (res.confirm) {
            return;
          }
        }
      })
    }

    wx.showModal({
      title: '提示',
      content: s,
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定');
          console.log(youxiao);
          let u = app.globalData.url + "weixiuxiangmu.php";

          let d = {
            uid: wx.getStorageSync('uid'),
            id: that.data.wxid,
            data: JSON.stringify(youxiao)
          };
          console.log(d);
          app.http(u, d).then(res => {
            wx.showToast({
              title: '成功，待审核',
              icon: 'success',
              duration: 2000
            })
            setTimeout(function() {
              wx.reLaunch({
                url: '/pages/weixiu/weixiu'
              })
            }, 2000)
          }).catch(res => {
            console.log('fail:', res);
          }).finally(() => {
            console.log('finally:', "结束");
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
    //const { index } = e.currentTarget.dataset;
    //const animation = this.data.animations[index];
    const animation = 'zoom';
    //onst classNames = `wux-animate--${animation}`;
    const classNames = `wux-animate--zoom`;

    var d = JSON.parse(options.d);
    var o = [];
    for (var i = 0; i < d.length; i++) {
      o.push(i);
    }

    this.setData({
      'example.in': true,
      'example.classNames': classNames,
      'example.animation': animation,
      wxid: options.id,
      weixiu: d,
      obj: o
    })
    wx.hideLoading();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
  quxiaocaozuo: function() {
    wx.reLaunch({
      url: '/pages/weixiu/weixiu'
    })
  },

  /**
   * 动画
   */
  onEnter(e) {
    console.log('onEnter', e.detail)
  },
  onEntering(e) {
    console.log('onEntering', e.detail)
  },
  onEntered(e) {
    console.log('onEntered', e.detail)

    // delay 300ms close animation
    setTimeout(() => this.setData({
      //'example.in': false
    }), 300)
  },
  onExit() {
    console.log('onExit')
  },
  onExiting() {
    console.log('onExiting')
  },
  onExited() {
    console.log('onExited')
  },
  onChange(e) {
    console.log('onChange', e.detail)
  },
  /**
   * 动画
   */
})