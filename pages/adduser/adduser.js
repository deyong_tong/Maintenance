const app = getApp();
const isTel = (value) => !/^1[3456789]\d{9}$/.test(value);
var istj = false;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    dw: '',
    dwid: 0,
    lxr: '',
    tel: ''

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    var u = app.globalData.url + "getdanwei.php"
    var d = {
      uid: wx.getStorageSync('uid')
    }
    app.http(u, d).then(res => {
      that.setData({
        dw: res.dw,
        dwid: res.dwid
      })
    }).catch(res => {
      console.log('fail:', res);
    }).finally(() => {
      console.log('finally:', "结束");
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
  clxr: function(e) {
    this.setData({
      lxr: e.detail.value
    })
  },
  ctel: function(e) {
    this.setData({
      tel: e.detail.value
    })
  },
  okbtn: function() {
    if (istj) {
      return false;
    }
    istj = true;
    var that = this;
    var lxr = that.data.lxr;
    var tel = that.data.tel;
    var dwid = that.data.dwid;
    console.log(lxr, tel, dwid)
    if (lxr.length < 2 || dwid <= 0 || isTel(tel)) {
      wx.showToast({
        icon: 'none',
        title: '数据非法'
      })
      istj = false;
      return false;
    } else {
      var u = app.globalData.url + "addusers.php"
      var d = {
        dwid: dwid,
        name: lxr,
        tel: tel
      }
      app.http(u, d).then(res => {
        wx.showModal({
          title: '添加成功',
          content: '请分享小程序给好友，邀请其加入。',
          showCancel: false,
          success(res) {
            if (res.confirm) {
              wx.navigateBack({
                delta: 1
              })
            }
          }
        })
      }).catch(res => {
        console.log('fail:', res);
      }).finally(() => {
        console.log('finally:', "结束");
        istj = false;
      })
    }
  }
})