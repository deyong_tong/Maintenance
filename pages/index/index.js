//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    zt: 100
  },
  onLoad: function () {
    //判断是用户是否绑定了
    var that = this;
    if (app.globalData.employId && app.globalData.employId != '') {
      var zc = wx.getStorageSync('config');
      var zt = wx.getStorageSync('zt');
      console.log(zt, "ok")
      if (zt == 0) {
        //未注册
        if (zc.isreg != 1) {
          wx.reLaunch({
            url: '/pages/welcome/welcome'
          })
          return false;
        }else{
          //未注册，前往注册页面
          wx.reLaunch({
            url: '/pages/reg/reg'
          })
        }
      } else if (zt == 1) {
        //前往报修者页面
        wx.reLaunch({
          url: '/pages/baoxiu/baoxiu'
        })
      } else if (zt == 2) {
        //前往管理者页面
        wx.reLaunch({
          url: '/pages/guanli/guanli'
        })
      } else if (zt == 3) {
        //前往维修者页面
        wx.reLaunch({
          url: '/pages/weixiu/weixiu'
        })
      } else {
        that.setData({
          zt: zt
        })
        wx.hideLoading();
      }
    } else {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.employIdCallback = employId => {
        if (employId != '') {
          var zc = wx.getStorageSync('config');
          var zt = wx.getStorageSync('zt');
          console.log(zt, "no")
          if (zt == 0) {
            if (zc.isreg != 1) {
              wx.reLaunch({
                url: '/pages/welcome/welcome'
              })
              return false;
            }else{
              //未注册，前往注册页面
              wx.reLaunch({
                url: '/pages/reg/reg'
              })
            }
          } else if (zt == 1) {
            //前往报修者页面
            wx.reLaunch({
              url: '/pages/baoxiu/baoxiu'
            })
          } else if (zt == 2) {
            //前往管理者页面
            wx.reLaunch({
              url: '/pages/guanli/guanli'
            })
          } else if (zt == 3) {
            //前往维修者页面
            wx.reLaunch({
              url: '/pages/weixiu/weixiu'
            })
          } else {
            that.setData({
              zt: zt
            })
            wx.hideLoading();
          }
        }
      }
    }
  }
})